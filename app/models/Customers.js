const mongoose = require('mongoose')
const Schema = mongoose.Schema
const customersSchema = new Schema({
  name: String,
  address: String,
  dob: Date,
  gender: String,
  mobile: String,
  email: String
})

const Customers = mongoose.model('customers', customersSchema)

Customers.find(function (err, customers) {
  if (err) return console.error(err)
  console.log(customers)
})

module.exports = mongoose.model('customers', customersSchema)
